from urllib.parse import urlencode
import json

def call(client, path, params):
    url = path + '?' + urlencode(params)
    response = client.get(url)
    return json.loads(response.data.decode('utf-8'))

def test_plus_eins(client):
    result = call(client, '/plus_eins', {'x': 2})
    assert result['x'] == 3

def test_quadrat(client):
    result = call(client, '/quadrat', {'x': 2})
    assert result['x'] == 4
