import os
from flask import request, Flask

app = Flask(__name__)

@app.route('/reload')
def reload():
    if request.args.get('password') == '123':
        try:
            os.system('git pull origin master')
            return 'Erfolg'
        except:
            return 'Fehler'
    else:
        return 'Falsches Passwort'
